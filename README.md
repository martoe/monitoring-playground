# System Monitoring 101


* [cAdvisor UI](http://localhost:8080/containers/)
* [Prometheus metrics](http://localhost:9090/metrics)
* [Prometheus exporessiion browser](http://localhost:9090/graph)


## Metrics generation

* [cAdvisor](https://github.com/google/cadvisor) - container monitoring
    * can write to InfluxDB
    * can be scraped by [Prometheus](https://github.com/google/cadvisor/blob/master/docs/storage/prometheus.md)
    * [c't intro](https://www.heise.de/select/ct/2019/6/1552302381801818)
*	nodeexporter - ???

## Data collection

* [Prometheus](https://prometheus.io/docs/introduction/overview/) - data collector and time series database (short-term storage only)
	* [Prometheus introduction](https://www.youtube.com/watch?v=PDxcEzu62jk)
* [influxdb](https://docs.influxdata.com/influxdb/) - time series database
    * [c't intro](https://www.heise.de/select/ct/2019/5/1551091687444779)

## Data representation

* Grafana - reads from influxdb, prometheus
    * [c't intro](https://www.heise.de/select/ct/2019/10/1556885151323343)

## alarme

*	grafana

## ???

* telegraf
* alertmanager
