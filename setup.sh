#!/bin/bash

mkdir -p data/prometheus
chmod o+w data/prometheus

mkdir -p data/grafana
chmod o+w data/grafana

mkdir -p data/influx
